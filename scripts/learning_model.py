#!/usr/bin/env
from hrvanalysis import remove_outliers, interpolate_nan_values, remove_ectopic_beats
from hrvanalysis import extract_features
from hrv_helper_functions import extract_hrv_features, feature_scaling, get_response_data, initializer, fit_reg_spline
import pandas as pd
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


'''
Set required variables here
del_t - defines time window for feature extraction
test_data_lim - test/train split
'''
ibi_files, performance_files, features_df, perf_df  =initializer()
test_data_lim = 100
del_t = 180

for f in ibi_files:    
    # extract features and performance data
    features_df, performance_data, ibi_new, perf_df = extract_hrv_features(f, features_df, performance_files, perf_df, del_t)       

drop_pos = features_df.index[np.isinf(features_df).any(1)]

features_df = features_df.drop(drop_pos)
perf_df = perf_df.drop(drop_pos)

'''
Correlation check
'''
dummy_df = features_df
dummy_df['target'] = perf_df['delay']
dummy_df = dummy_df.drop(['feature_time_stamp'], axis=1)

fig, axs = plt.subplots(1, 2, figsize=(30,5))
cor = dummy_df.corr(method= 'pearson')
sns.heatmap(cor, annot=True, cmap=plt.cm.Reds, ax = axs[0])
axs[0].set_title('Pearson')

cor2 = dummy_df.corr(method= 'spearman')
sns.heatmap(cor2, annot=True, cmap=plt.cm.Greens, ax = axs[1])
axs[1].set_title('Spearman')
plt.show()

'''
Multi-layer neural network for regression
'''

nn = MLPRegressor(
    hidden_layer_sizes=(3,),  activation='relu', solver='adam', alpha=0.001, batch_size='auto',
    learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
    random_state=0, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
    early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)    

nn = nn.fit(features_df[0:test_data_lim], perf_df['delay'][0:test_data_lim]) 
y_est = nn.predict(features_df[test_data_lim:])

plt.scatter(perf_df['time_stamp'][test_data_lim:],y_est, label = 'Estimate')
plt.scatter(perf_df['time_stamp'][test_data_lim:],perf_df['delay'][test_data_lim:], label = 'True')

'''
Fit regularized spline to true, and estimated performancee
'''
est_x, est_y = fit_reg_spline(perf_df['time_stamp'][test_data_lim:],y_est)
tru_x, tru_y = fit_reg_spline(perf_df['time_stamp'][test_data_lim:], perf_df['delay'][test_data_lim:])

plt.plot(tru_x, tru_y, linewidth = '3', alpha = 0.5)
plt.plot(est_x, est_y, linewidth = '3', alpha = 0.5)

plt.xlabel('time (s)')
plt.ylabel('delay (ms)')
plt.title('True response vs. estimated response')

plt.legend()
plt.show()



