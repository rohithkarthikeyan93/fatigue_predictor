import sys
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from scipy import stats
import helper_functions as hf
import os
import glob
from sklearn.preprocessing import MinMaxScaler

def get_response_data(file_name):
    in_data = pd.read_csv(file_name)
    q_low = in_data["response_delay"].quantile(0.1)
    q_hi  = in_data["response_delay"].quantile(0.9)

    new_df = in_data[(in_data["response_delay"] < q_hi) & (in_data["response_delay"] > q_low)]
    new_df = new_df.dropna(subset=['response_delay'])

    new_df = new_df.drop(new_df.index[new_df['incorrect_response']==1])
    
    x = new_df['absolute_response_time']    
    y = new_df['response_delay']
    
    return x,y

file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/thirty/'
os.chdir(file_path)
extension = 'csv'
file_names = glob.glob('*.{}'.format(extension))

fig = plt.figure()
y_values_complete = []
x_values_complete = []
errors_complete =[]
means_complete = []
count_array = []
counter = 0
for f in file_names:
    x,y = get_response_data(f)
    y = y.values
    y = y.reshape(-1,1)
    
    # exclusion critera
    if y.mean() < 500: #TODO: set desired interval to first 5 minutes
        scaler = MinMaxScaler()

        y_scaled = scaler.fit_transform(y)
        plt.scatter(x,y_scaled,color = 'g',  alpha = 0.4)
        y_values_complete = np.append(y_values_complete, y_scaled)
        x_values_complete = np.append(x_values_complete, x.values.reshape(-1,1))
        means_complete = np.append(means_complete, y.mean())
        errors_complete = np.append(errors_complete, y.std())
        counter+=1
        count_array = np.append(count_array, counter)

x_range = np.arange(x_values_complete.min(),x_values_complete.max(), 0.5)
spline_model = hf.get_natural_cubic_spline_model(x_values_complete, 
y_values_complete, x_values_complete.min(),x_values_complete.max(), n_knots=3)
y_est = spline_model.predict(x_range)
plt.plot(x_range,y_est, 'r', linewidth = '4', label = 'Trend')


plt.ylabel('Normalized response delay')
plt.xlabel('Time (s)')
plt.title('Response delay across '+str(counter)+ ' participants')


fig.savefig(file_path+'summary_stats.png')
plt.show()


plt.errorbar(count_array, means_complete,yerr = errors_complete)

plt.ylabel('Mean response delay (ms)')
plt.xlabel('Participant #')
plt.title('Mean response delay across participants')
plt.show()