import cx_Freeze
executables = [cx_Freeze.Executable("mod_pvt.py")]


cx_Freeze.setup(
    name="PVT",
    options={"build_exe": {"packages":["pygame"]}},
    executables = executables

    )